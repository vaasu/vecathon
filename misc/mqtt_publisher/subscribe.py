
import paho.mqtt.client as mqtt
import time
current_emotion = 0
emotions = ["happy","neutral","sad","love"]
broker = "192.168.0.87"
port = 1883

def main():
    client = mqtt.Client("control2")
    client.connect(broker,port)
    client.on_message = on_publish
    topic = "/1/emotion"
    client.subscribe(topic)
    client.loop_start()
    while (True):
        print("waiting for  data on topic'" + topic +"'")

       # e = get_emotion()
        #print(e)
        #print(get_emotion())
        #client.publish("/emotion",e)

        time.sleep(1)
    pass

def on_publish(client,userdata,result):
    print("data published ")
    print("userdata = " + str(result.payload))
    print(result.__dict__)
    #print(result.payload)



def get_emotion():
    global current_emotion
    current_emotion = (current_emotion + 1) % 4

    return emotions[current_emotion]


main()