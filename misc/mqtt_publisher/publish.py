#    "arduino.additionalUrls": "http://arduino.esp8266.com/stable/package_esp8266com_index.json"

import paho.mqtt.client as mqtt
import time
current_emotion = 0
emotions = ["happy","neutral","sad","love","fire"]
broker = "192.168.0.87"
port = 1883

def main():
    client = mqtt.Client("control1")
    client.connect(broker,port)
    while (True):
        print("publishing emotion.. ")
        e = get_emotion()
        print(e)
        #print(get_emotion())
        client.publish("/plant1/emotion",e)

        time.sleep(3)
    pass




def get_emotion():
    global current_emotion
    current_emotion = (current_emotion + 1) % len(emotions)

    return emotions[current_emotion]


main()
