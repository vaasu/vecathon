#include <MD_MAX72xx.h>
#include <SPI.h>

#include <MD_MAX72xx.h>
#include <SPI.h>

/*
 Basic ESP8266 MQTT example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.

 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <string>



 byte IMAGES[][8] = {
{
  B00000000, // x index = 0 , happy face (not used)
  B01100110,
  B00000000,
  B00011000,
  B10000001,
  B10000001,
  B01100110,
  B00111100
},{ 
  B00000000, // h   index = 1
  B01100110,
  B01100110,
  B00000000,
  B10000001,
  B11000011,
  B01100110,
  B00111100
},{
  B00000000,  //s index=2
  B01100110,
  B01100110,
  B00000000,
  B00111100,
  B01100110,
  B11000011,
  B10000001
},{ 
  B00000000,  //l index=3
  B01100110,
  B11111111,
  B11111111,
  B11111111,
  B01111110,
  B00111100,
  B00011000
},{
  B00000000,   //n index=4
  B01100110,
  B01100110,
  B00000000,
  B00000000,
  B01111110,
  B00000000,
  B00000000
},{
  B00000000,   //x index = 5 ( not used)
  B01100110,
  B01100110,
  B00000000,
  B00000000,
  B01111110,
  B00000000,
  B00000000
},{
  B10011001,   //f index = 6
  B01100110,
  B01100110,
  B10011001,
  B00000000,
  B01111110,
  B11000011,
  B10000001
}};
 int IMAGES_LEN = sizeof(IMAGES)/8;





// Update these with values suitable for your network.

const char* ssid = "VI VECATHON";
const char* password = "vecathon2019";
const char* mqtt_server = "192.168.0.87";


#define MAX_TOPIC_LENGTH 255
const char* plant_number = "1";
char subscribing_topic[MAX_TOPIC_LENGTH];

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

// Define the number of devices we have in the chain and the hardware interface
// NOTE: These pin numbers will probably not work with your hardware and may
// need to be adapted
#define  MAX_DEVICES 1

#define CLK_PIN   D5 // or SCK
#define DATA_PIN  D7 // or MOSI
#define CS_PIN    D8 // or SS

// SPI hardware interface
//MD_MAX72XX mx = MD_MAX72XX(CS_PIN, MAX_DEVICES);
#define HARDWARE_TYPE MD_MAX72XX::GENERIC_HW  //edit this as per your LED matrix hardware type
MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  //n s l h
  if (length == 0) {
    return;
  }
  int image_num = 9;
  switch (payload[0])
  {
    case 'n':
    image_num = 4;
    break;
    case 'h':
    image_num = 1;
    break;
    case 's':
    image_num = 2;
    break;
    case 'f':
    image_num = 6;
    break;
    case 'l':
    image_num = 3;
    break;
    default:
    image_num = 1;
    break;
  }
  mx.clear();
  mx.setBuffer(7,8,&IMAGES[image_num][0]);
  
  
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();


}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, subscribe to the plants emotion topic...
      snprintf(subscribing_topic,MAX_TOPIC_LENGTH,"/plant%s/emotion",plant_number);
      Serial.print("subscribing to...");
      Serial.println(subscribing_topic);
      client.subscribe(subscribing_topic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  mx.begin();
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}
